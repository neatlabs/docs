# Documentation #

This repository hosts the links to documentation pages related to different NEATLabs projects.

### Recording of LFP and Video data using Open Ephys ###
### Offline animal data analysis and visualization using MATLAB ###
### Online animal data analysis and visualization using Simulink ###